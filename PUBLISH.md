The package is published to:
https://www.npmjs.com/package/atlassian-jwt

Ask one of the contributors (see package.json) to give you permission to publish.

Login:
`npm login --registry=https://registry.npmjs.org`

Publish (see package.json):
`npm run build-and-publish`
